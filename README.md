# README #

This application created for the simple connection to ssh server and use rdesktop application more easier.
This application made by shell script, and you can use this if you have any linux discribution.


### How do I get set up? ###

First step, you have to open server runnable file with any text editor. You must fill the address, port, and user_name variable value.
If the file is not runnable, you have to use chmod command to change that.

Last step, you can move the file in your /bin/ directory to get anywhere in the terminal.